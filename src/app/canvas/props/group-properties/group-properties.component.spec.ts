import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupPropertiesComponent } from './group-properties.component';

describe('GroupPropertiesComponent', () => {
  let component: GroupPropertiesComponent;
  let fixture: ComponentFixture<GroupPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
