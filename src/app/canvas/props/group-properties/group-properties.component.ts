import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Gradient, Pattern } from '../../../data/shape';
import { CanvasDirective } from '../../canvas.directive';
import { EditorService } from '../../editor/editor.service';
import { MatSelect, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { ColorPickerComponent } from '../color-picker/color-picker.component';
import { Group } from '../../../data/group';
import { Shape } from '../../../data/shape';

@Component({
  selector: 'app-group-properties',
  templateUrl: './group-properties.component.html',
  styleUrls: ['./group-properties.component.css'],
  entryComponents: [ ColorPickerComponent ]
})
export class GroupPropertiesComponent implements OnInit, OnDestroy {

  shapeSelectionChangedSubscription: Subscription;

  x: number;
  y: number;
  scale_x: number;
  scale_y: number;
  rotation: number;
  transparency: number;

  fill: string | Gradient | Pattern;
  stroke: string | Gradient;
  lineWidth: number;

  @Input() canvas: CanvasDirective;

  private shape: Group;

  constructor(public editorService: EditorService, public dialog: MatDialog) {}

  ngOnInit() {
    this.setShape();
    // atualiza o painel de propriedades quando o usuário interagir com as shapes direto no canvas
    this.shapeSelectionChangedSubscription = this.editorService.shapeSelectionChanged$.subscribe(shape => {
      // console.log(shape);
      if (shape) {
        this.setShape();
      }
    });
  }

  ngOnDestroy() {
    this.shapeSelectionChangedSubscription.unsubscribe();
  }

  setShape() {
    this.shape = this.editorService.selectedShape as Group;
    this.setFieldValues();
    // console.log('set shape');
    // console.log(this.shape);
    // console.log('end');
  }

  setFieldValues() {
    console.log(this.shape.id);
    this.x = this.shape.x;
    this.y = this.shape.y;
    this.scale_x = this.shape.max.x;
    this.scale_y = this.shape.max.y;
    this.rotation = this.shape.rotation;
    this.transparency = this.shape.style.transparency * 100;

    this.fill = this.shape.style.fill;

    this.lineWidth = this.shape.style.lineWidth;
    this.stroke = this.shape.style.stroke;
  }

  updateShape(): void {
    this.shape.moveTo(Number(this.x), Number(this.y));
    this.shape.scale(this.scale_x, this.scale_y);
    this.shape.rotation = this.rotation;
    this.shape.style.transparency = this.transparency / 100;

    this.shape.style.fill = this.fill;

    this.shape.style.lineWidth = this.lineWidth;
    this.shape.style.stroke = this.stroke;

    this.canvas.figure.refresh();
    console.log(this.shape.id + ' updated');
  }

  openColorPickerFill(): void {
    const dialogRef = this.dialog.open(ColorPickerComponent, {
      width: '250px',
      data: { hex: this.fill }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.fill = result;
        this.updateShape();
      }
    });
  }

  openColorPickerStroke(): void {
    const dialogRef = this.dialog.open(ColorPickerComponent, {
      width: '250px',
      data: { hex: this.stroke }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.stroke = result;
        this.updateShape();
      }
    });
  }
}
